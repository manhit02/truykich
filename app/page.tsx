"use client";
import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Grid } from "swiper/modules";
import "swiper/css/navigation";
import "swiper/css/grid";
import "swiper/css";
export default function Home() {
  return (
    <main>
      <section>
        <div className="container m-auto">
          <div className="flex">
            <div className="basis-full z-10">
              <img
                className="absolute top-10 -z-10"
                src={"./assets/images/btn-5.png"}
                alt=""
              />
              <div className="pt-[440px] flex justify-center flex-col items-center app-mb">
                <img src={"./assets/images/logo-1.png"} alt="" />
                <img src={"./assets/images/title-1.png"} alt="" />
                <Link className="app-dl" href="">
                  tải game miễn phí
                </Link>
              </div>
              <div className="app-code">
                <div className="app-code__p">
                  Giftcode trải nghiệm:
                  <strong>2TAY2SUNG,CODEVIP999,CODEVIP888,TKPCVTC.</strong>
                  <br />
                  Gia nhập chiến trường Truy Kích PC - đồng hành cùng các xạ thủ
                  nữ vừa xinh vừa bắn hay.
                </div>
              </div>
              <div className="app-code__scroll">
                <img
                  className="app-code__scroll-img"
                  src="./assets/images/btn-3.png"
                />
                <p className="app-code__scroll-p">cuộn xuống để xem thêm</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section>
        <div className="container m-auto relative">
          <img
            src="./assets/images/title-2.png"
            alt=""
            className="app-title1 m-auto"
          />
          <Swiper
            modules={[Navigation]}
            navigation={{
              prevEl: ".mySwiper-control__prev",
              nextEl: ".mySwiper-control__next",
            }}
            className="mySwiper"
            loop={true}
            slideToClickedSlide
            slidesPerView={2}    
            spaceBetween={30}
            centeredSlides
            autoplay={{
              delay: 5000,
              disableOnInteraction: false,
            }}
    breakpoints={
      {
        768:{
          slidesPerView:3
  
        }}

      
    }
            
          >
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper-slide">
                <img
                  className="mySwiper-slide__img"
                  src="./assets/images/img-1.png"
                  alt=""
                />
              </Link>
              <div className="mySwiper-control__name">độ mixi1</div>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper-slide">
                <img
                  className="mySwiper-slide__img"
                  src="./assets/images/img-1.png"
                  alt=""
                />
              </Link>
              <div className="mySwiper-control__name">độ mixi2</div>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper-slide">
                <img
                  className="mySwiper-slide__img"
                  src="./assets/images/img-1.png"
                  alt=""
                />
              </Link>
              <div className="mySwiper-control__name">độ mixi3</div>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper-slide">
                <img
                  className="mySwiper-slide__img"
                  src="./assets/images/img-1.png"
                  alt=""
                />
              </Link>
              <div className="mySwiper-control__name">độ mixi4</div>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper-slide">
                <img
                  className="mySwiper-slide__img"
                  src="./assets/images/img-1.png"
                  alt=""
                />
              </Link>
              <div className="mySwiper-control__name">độ mixi5</div>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper-slide">
                <img
                  className="mySwiper-slide__img"
                  src="./assets/images/img-1.png"
                  alt=""
                />
              </Link>
              <div className="mySwiper-control__name">độ mixi6</div>
            </SwiperSlide>
          </Swiper>
          <div className="mySwiper-control">
            <Link href="" className="mySwiper-control__prev">
              <img src="./assets/images/btn-4.png" alt="" />
            </Link>
            {/* <Swiper modules={[Navigation]} className="mySwiper3 w-[50%]">
              <SwiperSlide>
                <div className="mySwiper-control__name">độ mixi1</div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="mySwiper-control__name">độ mixi2</div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="mySwiper-control__name">độ mixi3</div>
              </SwiperSlide>
              <SwiperSlide>
                <div className="mySwiper-control__name">độ mixi4</div>
              </SwiperSlide>
            </Swiper> */}
            <Link href="" className="mySwiper-control__next">
              <img src="./assets/images/btn-4.png" alt="" />
            </Link>
          </div>
        </div>
      </section>
      <section>
        <div className="container m-auto relative">
          <img
            src="./assets/images/title-3.png"
            alt=""
            className="app-title2 m-auto"
          />
          <Swiper
            modules={[Navigation, Grid]}
            navigation={{
              prevEl: ".mySwiper2-control__prev",
              nextEl: ".mySwiper2-control__next",
            }}
            className="mySwiper2"
         

            slidesPerView={1}   
          loop
            spaceBetween={30}
     centeredSlides
     autoplay={{
      delay: 5000,
      disableOnInteraction: false,
    }}
            breakpoints={{
              768: {
                loop:false,
                centeredSlides:false,
                slidesPerView: 2,
                grid: {
                  rows: 2
                },
              }
            }}
          >
            <SwiperSlide>
          

              <Link href="javascript:0;" className="mySwiper2-slide ">
    
                <iframe
                  className="mySwiper2-slide__iframe"
                  id="mySwiper2-slide__for"
                  src="https://www.youtube.com/embed/BzAJVC_0Eq4"
                ></iframe>
              </Link>
    
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper2-slide ">
                
                <iframe
                  className="mySwiper2-slide__iframe"
                  src="https://www.youtube.com/embed/BzAJVC_0Eq4"
                ></iframe>
              </Link>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper2-slide ">
                
                <iframe
                  className="mySwiper2-slide__iframe"
                  src="https://www.youtube.com/embed/BzAJVC_0Eq4"
                ></iframe>
              </Link>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper2-slide ">
                
                <iframe
                  className="mySwiper2-slide__iframe"
                  src="https://www.youtube.com/embed/BzAJVC_0Eq4"
                ></iframe>
              </Link>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper2-slide ">
                
                <iframe
                  className="mySwiper2-slide__iframe"
                  src="https://www.youtube.com/embed/BzAJVC_0Eq4"
                ></iframe>
              </Link>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper2-slide ">
                
                <iframe
                  className="mySwiper2-slide__iframe"
                  src="https://www.youtube.com/embed/BzAJVC_0Eq4"
                ></iframe>
              </Link>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper2-slide ">
                
                <iframe
                  className="mySwiper2-slide__iframe"
                  src="https://www.youtube.com/embed/BzAJVC_0Eq4"
                ></iframe>
              </Link>
            </SwiperSlide>
            <SwiperSlide>
              <Link href="javascript:0;" className="mySwiper2-slide ">
                
                <iframe
                  className="mySwiper2-slide__iframe"
                  src="https://www.youtube.com/embed/BzAJVC_0Eq4"
                ></iframe>
              </Link>
            </SwiperSlide>
          </Swiper>

          <Link href="javascript:0;" className="mySwiper2-control__prev">
            <img src="./assets/images/btn-4.png" alt="" />
          </Link>

          <Link href="javascript:0;" className="mySwiper2-control__next">
            <img src="./assets/images/btn-4.png" alt="" />
          </Link>
        </div>
      </section>
      <Link href="/" className="mov-top">
        <img src="./assets/images/btn-2.png" alt="" />{" "}
      </Link>
    </main>
  );
}
