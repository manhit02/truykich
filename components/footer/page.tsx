import Link from "next/link";

const Footer = () => {
  return (
    <footer className="footer bg-[#222529] py-4">
      <div className="container mx-auto">
        <div className="flex flex-row justify-center mb-3">
          <div className="basis-1/12 flex items-center mx-1  max-[768px]:basis-3/12">
            <img src="./assets/images/logo-3.png" alt="" />
          </div>
          <div className="basis-1/12 flex items-center mx-1 max-[768px]:basis-3/12">
            <img src="./assets/images/logo-4.png" alt="" />
          </div>

          <div className="basis-1/12 flex items-center mx-1 max-[768px]:basis-3/12">
            <img src="./assets/images/logo-2.png" alt="" />
          </div>
        </div>
        <p className="footer-p">
          Copright 2023 VTC Mobile. All rights reserved.
          <br />
          Công ty cổ phần VTC dịch vụ di động - Tầng 11 - Tòa nhà VTC Online, số
          18 Tam Trinh phường Minh Khai, quận Hai Bà Trưng, Hà Nội
          <br />
          SĐT: (84-4).39877470 Email: vtcmobile@vtc.vn
          <br />
          Giấy phép phê duyệt nội dung kịch bản trò chơi điện tử trên mạng số
          837/QĐ-BTTTT
          <br />
          Người chịu trách nhiệm quản lý nội dung: Ông Nguyễn Viết Quang Minh
          <br />
        </p>
        <div className="flex justify-center">
          <Link className="footer-btn1" href="">
            Chính sách bảo mật
          </Link>
          <Link className="footer-btn1" href="">
            Điều khoản sử dụng (VTC)
          </Link>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
